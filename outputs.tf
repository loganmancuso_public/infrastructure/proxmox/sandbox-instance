##############################################################################
#
# Author: Logan Mancuso
# Created: 06.22.2024
#
##############################################################################

output "properties" {
  description = "sandbox instance properties"
  value       = module.machine.properties
}
