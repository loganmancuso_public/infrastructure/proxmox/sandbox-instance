##############################################################################
#
# Author: Logan Mancuso
# Created: 06.24.2024
#
##############################################################################

## Provider ##
terraform {
  required_version = ">= 1.6.0"
  required_providers {
    proxmox = {
      source  = "bpg/proxmox"
      version = ">= 0.51.0"
    }
  }
}

provider "proxmox" {
  endpoint = "https://${local.node.ip}:8006/"
  username = "${local.credentials_proxmox.username}@pam"
  password = local.credentials_proxmox.password
  # (Optional) Skip TLS Verification
  insecure = true
  ssh {
    agent    = true
    username = local.credentials_proxmox.username
    node {
      name    = local.node.name
      address = local.node.ip
    }
  }
}