#!/bin/bash
##############################################################################
#
# Author: Logan Mancuso
# Created: 11.10.2023
#
##############################################################################

# Redirect all output to log file
exec > >(tee -a "${log}") 2>&1

# Helper function
function helper() {
  # Add the logic that the script will perform here
  echo -e "START:\thelper"
  echo -e "END:\thelper"
}

# Main function
function main() {
  echo -e "START:\tmain"
  echo "Waiting for cloud-init to finish"
  cloud-init status --wait
  # Display system information
  echo "System Information:"
  echo "hostname: $(cat /etc/hostname)"
  echo "cpu: $(top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1"%"}')"
  echo "memory: $(free | grep Mem | awk '{print $3/$2 * 100.0"%"}')"
  echo "user: $(whoami)"
  echo "time: $(date)"
  helper
  echo -e "END:\tmain"
}

# Start the script
main "$@"