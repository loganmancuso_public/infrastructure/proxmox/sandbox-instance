##############################################################################
#
# Author: Logan Mancuso
# Created: 06.24.2024
#
##############################################################################

data "terraform_remote_state" "datacenter" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/XXXXXXXX/terraform/state/${local.config.env}"
  }
}

data "terraform_remote_state" "global_secrets" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/XXXXXXXX/terraform/state/global-secrets"
  }
}

locals {
  # datacenter
  node     = data.terraform_remote_state.datacenter.outputs.nodes[local.config.node]
  networks = data.terraform_remote_state.datacenter.outputs.networks
  ipsets   = data.terraform_remote_state.datacenter.outputs.ipsets
  # global_secrets
  secret_proxmox = data.terraform_remote_state.global_secrets.outputs.proxmox
}

## Obtain Vault Secrets ##
data "vault_kv_secret_v2" "proxmox" {
  mount = local.secret_proxmox.mount
  name  = local.secret_proxmox.name
}

locals {
  credentials_proxmox = jsondecode(data.vault_kv_secret_v2.proxmox.data_json)
}